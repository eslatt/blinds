## When all of the blinds stop moving automatically

* can you ssh into the controller box?
* does crontab have the proper entries?
* was the log file touched the last time the cron'd command was supposed to run?
  * use the cron'd command to determine where the log file is
* was the state file touched the last time the cron'd command was supposed to run?
  * if not, something probably broke when the command was run
* invoke the cron'd command manually to see if there is stderror



## On *Wide character in subroutine entry at blinds.pm line 62.* error after power outage

```
my $forecast_ref = decode_json($json_content);
```

this is it trying to decode the weather that comes back from 

*Support for the Dark Sky API ended on March 31, 2023, and has been replaced by Apple’s WeatherKit API. Developers can use WeatherKit in apps with iOS 16, iPadOS 16, macOS 13, tvOS 16, and watchOS 9 or higher with a platform-specific Swift API, and on any other platform with a REST API. Learn more about WeatherKit.*

so it looks like decode_json is getting something it didn't expect

so I removed the state file.. and got the same error.. so maybe this is th econfig fiel and not the state file
