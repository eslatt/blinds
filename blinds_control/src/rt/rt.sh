#!/bin/bash

# to be run from cicd automation and not crontab
# in cicd automation I land in the root of the replository
# when I run this manually via docker run, I'll volume map the repo into /to-be-unknown/blinds
# and cd into /to-be-unknown/blinds to make it representative of the automation
# cwd is to be in repo root beside blinds_control
# cd to
# docker run -it -v "/Volumes/BOOTCAMP/edgitlab/1504 blinds/":"/to-be-unknown/blinds" registry.gitlab.com/eslatt/blinds:latest bash
# cd /to-be-unknown/blinds/blinds_control/src
# chmod +x ./rt/rt.sh
# ./rt/rt.sh

# questions
#  how do I get the numbers for the date times?
#   see 1 blinds_control.xlsx conversions 2022
#   a time string also comes out in the perl stdout when you run a time
#  how do I see what is happending in the csv output?

audit_dir=./audit

function reset_audit_dir() {
  rm -rf $audit_dir &>/dev/null
  mkdir $audit_dir
}

function blinds_control_p1() {
  echo blinds_control_p1 $1
  ./blinds_control_p1.pl ./rt/blinds.conf $1
  cp $audit_dir/blinds.state $audit_dir/blinds.state.$1
}

function single_time() {
  rm $audit_dir/blinds.state &>/dev/null
  blinds_control_p1 $1
}

function loop_time() {
  from_time=$1
  to_time=$2
  rm $audit_dir/blinds.state &>/dev/null

  for (( t=$from_time; t<=$to_time; t+=300 ))
  do  
    echo " loop_time $t"
    blinds_control_p1 $t
  done
}

reset_audit_dir

echo '1 blinds_control.xlsx conversions 2022' >> $audit_dir/blinds_control_p1.csv
echo '[$-en-US]ddd yyyy-mmm-dd hhmm;@' >> $audit_dir/blinds_control_p1.csv
echo time,excel time,cloud,temp,desired_irradiance_wmp2,irradiance_direct_beam_wpm2,elevation_rad,irradiance_clouded_wpm2,irradiance_window_wpm2-0,200xdesired_light_state-0,irradiance_window_wpm2-1,300xdesired_light_state-1 >> $audit_dir/blinds_control_p1.csv

single_time 1430150436
single_time 1434893436
single_time 1434902436
single_time 1434922236

 # 2015-05-09 75 degrees, 4ET (1431158418) - 10ET (1431223218)
loop_time 1431158418 1431223218

 # 2015-05-10 65 degrees, 6ET (1431252018) - 10ET (1431309618)
loop_time 1431252018 1431309618
