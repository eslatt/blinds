#!/usr/bin/perl
use strict;

use blinds;
use JSON::XS;
my $json_coder = JSON::XS->new->pretty(1);
$json_coder->canonical(1);

my $ld=","; # log delimeter
my $test_time;

if(@ARGV == 1)
{
  $test_time = time();
}
elsif(@ARGV == 2)
{
  $test_time = 1*$ARGV[1];
}
else
{
  die(" usage: base_json.pl <json_input_file/> [<time/>]\r\n");
}

my $test_time_str = date_time_string($test_time);

my $input_filename = $ARGV[0];
# die ("Can't find $input_filename\r\n") if(!(-e $input_filename));
# die ("$input_filename must be a file\r\n") if(-d $input_filename);

my $g_conf_ref  = read_conf($input_filename);
my $g_state_ref = read_state($g_conf_ref);

 # my $csv_filename = "$g_conf_ref->{global}->{base_dir}/$g_conf_ref->{global}->{workspace_dir}/blinds_control_p1.csv";
my $csv_filename = "$g_conf_ref->{global}->{workspace_dir}/blinds_control_p1.csv";

printf<<end_of_literal;

 blinds control regression test
   input        $input_filename
   output       $g_conf_ref->{global}->{workspace_dir}/blinds.state
   time         $test_time [$test_time_str]
   csv_filename $csv_filename
   
end_of_literal


get_weather($test_time, $g_conf_ref->{'global'}, $g_state_ref->{'get_weather'});
calc_desired_irradiance($g_state_ref->{'get_weather'}, $g_conf_ref->{'desired_irradiance'}, $g_state_ref->{'calc_desired_irradiance'});
calc_irradiance($test_time, $g_conf_ref->{'global'}, $g_state_ref->{'calc_irradiance'});
calc_desired_blinds_states($g_state_ref->{'get_weather'}, $g_state_ref->{'calc_desired_irradiance'}, $g_state_ref->{'calc_irradiance'}, $g_conf_ref->{'blinds'}, $g_state_ref->{'calc_desired_blinds_states'});
service_blinds($g_conf_ref->{'global'}, $g_conf_ref->{'blinds'}, $g_state_ref->{'calc_desired_blinds_states'}, $g_state_ref->{'service_blinds'});
write_state($g_state_ref);

printf(" irradiance_direct_beam_wpm2 $g_state_ref->{calc_irradiance}->{irradiance_direct_beam_wpm2}\r\n");
printf(" irradiance_window_wpm2      $g_state_ref->{calc_desired_blinds_states}->{blinds}->[0]->{irradiance_window_wpm2}\r\n");
printf(" desired_light_state         $g_state_ref->{calc_desired_blinds_states}->{blinds}->[0]->{desired_light_state}\r\n\r\n\r\n");

my $command='';
if( $g_state_ref->{'service_blinds'}->{'blinds'}->[0]->{'last_command'}->{'datetime'} == $test_time )
{
  $command = $g_state_ref->{'service_blinds'}->{'blinds'}->[0]->{'last_command'}->{'light'};
  die if( $g_state_ref->{'service_blinds'}->{'blinds'}->[0]->{'last_command'}->{'irradiance_wmp2'} != $g_state_ref->{'calc_desired_blinds_states'}->{'blinds'}->[0]->{'irradiance_window_wpm2'} );
}

# if( -e $csv_filename )
# {
  printf(" update $csv_filename\r\n");
  
  open(FLOUT, ">>$csv_filename") or die("Can not open $csv_filename\r\n");
  binmode(FLOUT);
  my $excel_days = ($test_time-4*3600)/86400+25569;
  my $dls_0x200=200*$g_state_ref->{calc_desired_blinds_states}->{blinds}->[0]->{desired_light_state};
  my $dls_1x300=300*$g_state_ref->{calc_desired_blinds_states}->{blinds}->[1]->{desired_light_state};
  my $csv_entry =<<end_of_literal;
    $test_time${ld}
    $excel_days${ld}
    $g_state_ref->{get_weather}->{cloud_coverage_now}${ld}
    $g_state_ref->{get_weather}->{max_temp_f_today}${ld}
    $g_state_ref->{calc_desired_irradiance}->{desired_irradiance_wmp2}${ld}
    $g_state_ref->{calc_irradiance}->{irradiance_direct_beam_wpm2}${ld}
    $g_state_ref->{calc_irradiance}->{elevation_rad}${ld}
    $g_state_ref->{calc_desired_blinds_states}->{irradiance_clouded_wpm2}${ld}
    $g_state_ref->{calc_desired_blinds_states}->{blinds}->[0]->{irradiance_window_wpm2}${ld}
    $dls_0x200${ld}
    $g_state_ref->{calc_desired_blinds_states}->{blinds}->[1]->{irradiance_window_wpm2}${ld}
    $dls_1x300
end_of_literal

  $csv_entry =~ s/\s+//g;
  $csv_entry =~ s/\n//g;
  $csv_entry =~ s/\r//g;

  print(FLOUT "$csv_entry\r\n");
  close(FLOUT);
# }

sub read_file
{
  my ($file_name) = @_;

  my $contents;
  open(FLIN, $file_name) or die("Can not open $file_name\r\n");
  binmode(FLIN);
  read(FLIN, $contents, -s  FLIN);
  close(FLIN);

  return $contents;
}

sub write_file
{
  my ($file_name, $contents_ref) = @_;

  my $contents;
  open(FLOUT, ">$file_name") or die("Can not open $file_name\r\n");
  binmode(FLOUT);
  print(FLOUT ${$contents_ref});
  close(FLOUT)
}

