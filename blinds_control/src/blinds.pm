package blinds;
require Exporter;

@ISA = qw/Exporter/;
@EXPORT = qw/read_conf read_state write_state get_weather calc_desired_irradiance calc_irradiance calc_desired_blinds_states service_blinds date_time_string/;

use strict;

use LWP::UserAgent;
use HTTP::Request::Common qw(GET POST);

use Astro::Coord::ECI;
use Astro::Coord::ECI::Sun;
use Astro::Coord::ECI::Utils qw{:all};
use POSIX;

use JSON::XS;
my $json_coder = JSON::XS->new->pretty(1);
$json_coder->canonical(1);

use LWP::UserAgent;
use HTTP::Request::Common qw(GET POST);

my $g_agent=0;
my $g_conf_ref;

sub get_weather
{
  my ($datetime, $conf_global_ref, $state_get_weather_ref) = @_;

  my $json_content;
  my $hour = floor($datetime/3600);
  my $hour_filename;
  my $second_filename;
  if( exists( $conf_global_ref->{'canned_weather_dir'} ) )
  {
    $hour_filename = "$conf_global_ref->{canned_weather_dir}/canned.weather.${hour}h";
    $second_filename = "$conf_global_ref->{canned_weather_dir}/canned.weather.${datetime}s";
  }

  if   ( -e $second_filename && !( -d $second_filename ) )
  {
    printf(" get_weather using canned second weather for testing\n");
    $json_content = read_file( $second_filename );
  }
  elsif( -e $hour_filename && !( -d $hour_filename ) )
  {
    printf(" get_weather using canned hour weather for testing\n");
    $json_content = read_file( $hour_filename );
  }
  else
  {
   # $g_agent = new LWP::UserAgent if(!$g_agent);

   # my $action='https://api.forecast.io/forecast/c02c92892be6e2d9a4c6e19104c4d614/40.351366,-76.660661';
   # my $req = GET( $action );
   # my $res = $g_agent->request($req);
   # die("    failed to get %s $json_content\n", $res->as_string) if(!$res->is_success);
   # $json_content = $res->decoded_content;
   # this api finished 2023-03-31
    $json_content =<<end_of_literal;
    {
    }
end_of_literal
  }

  my $forecast_ref = decode_json($json_content);

  $state_get_weather_ref->{'datetime'} = 1*$datetime;
  $state_get_weather_ref->{'max_temp_f_today'} = $forecast_ref->{'daily'}->{'data'}->[0]->{'temperatureMax'};
  $state_get_weather_ref->{'max_temp_time_today'} = date_time_string($forecast_ref->{'daily'}->{'data'}->[0]->{'temperatureMaxTime'});
  $state_get_weather_ref->{'cloud_coverage_now'} = $forecast_ref->{'currently'}->{'cloudCover'};
  $state_get_weather_ref->{'pressure_now'} = $forecast_ref->{'currently'}->{'pressure'};
  $state_get_weather_ref->{'sunrise_time_today'} = date_time_string($forecast_ref->{'daily'}->{'data'}->[0]->{'sunriseTime'});
  $state_get_weather_ref->{'sunset_time_today'} = date_time_string($forecast_ref->{'daily'}->{'data'}->[0]->{'sunsetTime'});
}

sub calc_desired_irradiance
{
  my ($state_get_weather_ref, $conf_desired_irradiance_ref, $state_calc_desired_irradiance_ref) = @_;

 # y is wmp2, x is f
  my $m = ( $conf_desired_irradiance_ref->{'desired_irradiance_wmp2_2'} - $conf_desired_irradiance_ref->{'desired_irradiance_wmp2_1'} ) /
          ( $conf_desired_irradiance_ref->{'estimated_temp_f_2'} - $conf_desired_irradiance_ref->{'estimated_temp_f_1'} );
  my $b = $conf_desired_irradiance_ref->{'desired_irradiance_wmp2_1'} - $m * $conf_desired_irradiance_ref->{'estimated_temp_f_1'};
  $state_calc_desired_irradiance_ref->{'desired_irradiance_wmp2'} = $m * $state_get_weather_ref->{'max_temp_f_today'} + $b;
  $state_calc_desired_irradiance_ref->{'desired_irradiance_wmp2'} = 0 if( $state_calc_desired_irradiance_ref->{'desired_irradiance_wmp2'} < 0 );
}

sub calc_irradiance
{
  my ($datetime, $conf_global_ref, $state_calc_irradiance_ref) = @_;

  my $lat_rad = deg2rad ($conf_global_ref->{'loc_lat_degrees'});
  my $lon_rad = deg2rad ($conf_global_ref->{'loc_lon_degrees'});
  my $alt_km  = $conf_global_ref->{'loc_alt_km'};

  my $loc = Astro::Coord::ECI->     universal( $datetime )->geodetic ($lat_rad, $lon_rad, $alt_km);
  my $sun = Astro::Coord::ECI::Sun->universal( $datetime );
  my ($azimuth_rad, $elevation_rad, $range) = $loc->azel( $sun );
  my $zenith_rad = PI/2 - $elevation_rad;
  my $i_unit = cos($elevation_rad)*sin($azimuth_rad);
  my $j_unit = cos($elevation_rad)*cos($azimuth_rad);
  my $k_unit = sin($elevation_rad);

  $state_calc_irradiance_ref->{'datetime'}      = $datetime;
  $state_calc_irradiance_ref->{'azimuth_rad'}   = $azimuth_rad;
  $state_calc_irradiance_ref->{'elevation_rad'} = $elevation_rad;
  $state_calc_irradiance_ref->{'zenith_rad'}    = $zenith_rad;
  $state_calc_irradiance_ref->{'i_unit'}        = $i_unit;
  $state_calc_irradiance_ref->{'j_unit'}        = $j_unit;
  $state_calc_irradiance_ref->{'k_unit'}        = $k_unit;

  if( $elevation_rad > 0 )
  {
    my $air_mass = 1 / ( cos( $zenith_rad ) + 0.50572 * pow( rad2deg( 1.676911 - $zenith_rad ), -1.6364 ) );
    my $irradiance_direct_beam_wpm2 = 1353 * ( ( 1 - 0.14 * $alt_km ) * pow( 0.7, pow( $air_mass, 0.678 ) ) +  0.14 * $alt_km );
    $state_calc_irradiance_ref->{'air_mass'}=$air_mass;
    $state_calc_irradiance_ref->{'irradiance_direct_beam_wpm2'}=$irradiance_direct_beam_wpm2;
  }
  else
  {
    $state_calc_irradiance_ref->{'air_mass'}                    = -1;
    $state_calc_irradiance_ref->{'irradiance_direct_beam_wpm2'} = -1;
  }
}

sub calc_desired_blinds_states
{
  my ($state_get_weather_ref, $state_calc_desired_irradiance_ref, $state_calc_irradiance_ref, $conf_blinds, $calc_desired_blinds_states_ref ) = @_;

  $calc_desired_blinds_states_ref->{'datetime'}=$state_get_weather_ref->{'datetime'};
  if($state_calc_irradiance_ref->{'irradiance_direct_beam_wpm2'}==-1)
  {
    $calc_desired_blinds_states_ref->{'irradiance_clouded_wpm2'}=-1;
  }
  else
  {
    $calc_desired_blinds_states_ref->{'irradiance_clouded_wpm2'}=(1-$state_get_weather_ref->{'cloud_coverage_now'})*$state_calc_irradiance_ref->{'irradiance_direct_beam_wpm2'};
  }

  for( my $blind_index=0; $blind_index<int(@{$conf_blinds->{'blinds'}}); $blind_index++)
  {
    my $conf_blind_ref  = $conf_blinds->{'blinds'}->[$blind_index];
    $calc_desired_blinds_states_ref->{'blinds'}->[$blind_index] = {};
    my $state_blind_ref = $calc_desired_blinds_states_ref->{'blinds'}->[$blind_index];


    my $security_min_elevation_rad   = deg2rad($conf_blind_ref->{'security_min_elevation_degrees'});
    printf("elevation_rad $state_calc_irradiance_ref->{elevation_rad}\n");
    printf("security_min_elevation_rad $security_min_elevation_rad\n");
    if($state_calc_irradiance_ref->{'elevation_rad'}<$security_min_elevation_rad)
    {
      $state_blind_ref->{'irradiance_window_wpm2'} = -2;
      $state_blind_ref->{'desired_light_state'}    =  0;
    }
    elsif($calc_desired_blinds_states_ref->{'irradiance_clouded_wpm2'}==-1)
    {
      $state_blind_ref->{'irradiance_window_wpm2'} = -1;
      $state_blind_ref->{'desired_light_state'}    =  1;
    }
    else
    {
      my $elevation_rad = deg2rad($conf_blind_ref->{'elevation_degrees'});
      my $azimuth_rad   = deg2rad($conf_blind_ref->{'azimuth_degrees'});
      my $i_b_unit = cos($elevation_rad)*sin($azimuth_rad);
      my $j_b_unit = cos($elevation_rad)*cos($azimuth_rad);
      my $k_b_unit = sin($elevation_rad);

      my $blinds_dot_sun = $state_calc_irradiance_ref->{'i_unit'} * $i_b_unit +
                           $state_calc_irradiance_ref->{'j_unit'} * $j_b_unit +
                           $state_calc_irradiance_ref->{'j_unit'} * $k_b_unit;

      $state_blind_ref->{'irradiance_window_wpm2'} = $blinds_dot_sun * $calc_desired_blinds_states_ref->{'irradiance_clouded_wpm2'};

      if( $state_blind_ref->{'irradiance_window_wpm2'} <= 0 )
      {
        $state_blind_ref->{'irradiance_window_wpm2'} = 0;
        $state_blind_ref->{'desired_light_state'}    = 1;
      }
      elsif( exists($conf_blind_ref->{'shaded'}) && $conf_blind_ref->{'shaded'} eq 'true' )
      {
        $state_blind_ref->{'desired_light_state'}    =  1;
      }
      else
      {
        $state_blind_ref->{'desired_light_state'}    = $state_calc_desired_irradiance_ref->{'desired_irradiance_wmp2'} / $state_blind_ref->{'irradiance_window_wpm2'};
      }
      $state_blind_ref->{'desired_light_state'}=0 if($state_blind_ref->{'desired_light_state'}<0);
      $state_blind_ref->{'desired_light_state'}=1 if($state_blind_ref->{'desired_light_state'}>1);
    }
  }
}

sub send_command
{
  my ( $conf_global_ref, $conf_blinds_blind_ref, $service_blinds_blind_ref, $last_command_ref ) = @_;
  my $date_flag = '';
  $date_flag = "/t" if( $conf_global_ref->{'base_dir'} =~ m/^E/ );
  if($last_command_ref->{'light'}==1)
  {
   # my $command = "\"$conf_global_ref->{base_dir}/../../blinds_xbee/dist/light\"";
   # my $command = "\"$conf_global_ref->{base_dir}/../../XbeeGateway/light\"";
    my $command = "\"$conf_blinds_blind_ref->{light_command}\"";
    printf("send light command $command\n");
   # TODO fix following to use the workspace_dir
   # system("date $date_flag >> \"$conf_global_ref->{base_dir}/../audit/light.log\"");
    system($command); # . ' &');
  }
  elsif($last_command_ref->{'light'}==0)
  {
   # my $command = "\"$conf_global_ref->{base_dir}/../../blinds_xbee/dist/dark\"";
   # my $command = "\"$conf_global_ref->{base_dir}/../../XbeeGateway/dark\"";
    my $command = "\"$conf_blinds_blind_ref->{dark_command}\"";
    printf("send dark command $command\n");
   # TODO fix following to use the workspace_dir
   # system("date $date_flag >> \"$conf_global_ref->{base_dir}/../audit/dark.log\"");
    system($command); # . ' &');
  }
  else
  {
    die;
  }
  $service_blinds_blind_ref->{'last_command'}=$last_command_ref;
}

sub sb_service_blind
{
  my ($conf_global_ref, $conf_blinds_ref, $conf_blinds_blind_ref, $state_desired_blind_state_ref, $state_service_blind_ref, $calc_desired_blinds_states_datetime) = @_;

  my $binary_desired_light_state = 1;
  $binary_desired_light_state = 0 if( $state_desired_blind_state_ref->{'desired_light_state'} < 1);
  my $command_ref =
  {
    'light'           => $binary_desired_light_state,
    'irradiance_wmp2' => $state_desired_blind_state_ref->{'irradiance_window_wpm2'},
    'datetime'        => $calc_desired_blinds_states_datetime
  };

  if( !(exists( $state_service_blind_ref->{'last_command'} ) ) )
  {
    send_command( $conf_global_ref, $conf_blinds_blind_ref, $state_service_blind_ref, $command_ref );
    return;
  }

  return if( $binary_desired_light_state == $state_service_blind_ref->{'last_command'}->{'light'} );

  my $dwell_seconds = $calc_desired_blinds_states_datetime - $state_service_blind_ref->{'last_command'}->{'datetime'};
  
 # temp until firmware at the blind is updated to interupt movement; takes ~<30 min for blinds to come down
  return if( $dwell_seconds < 30 * 60 );
  
  my $dwell_wpm2 = $state_desired_blind_state_ref->{'irradiance_window_wpm2'} - $state_service_blind_ref->{'last_command'}->{'irradiance_window_wpm2'};
  $dwell_wpm2 *= -1 if( $dwell_wpm2 < 0 );
  return if( $dwell_wpm2 < $conf_blinds_ref->{'dwell_wmp2'} && $dwell_seconds < $conf_blinds_ref->{'dwell_seconds'} );

  send_command( $conf_global_ref, $conf_blinds_blind_ref, $state_service_blind_ref, $command_ref );
}

sub service_blinds
{
  my ($conf_global_ref, $conf_blinds_ref, $state_calc_desired_blinds_states_ref, $state_service_blinds_ref ) = @_;

  printf("service_blinds\n");

  for( my $blind_index=0; $blind_index<int(@{$state_calc_desired_blinds_states_ref->{'blinds'}}); $blind_index++)
  {
    $state_service_blinds_ref->{'blinds'}->[$blind_index] = {} if( !( exists( $state_service_blinds_ref->{'blinds'}->[$blind_index] ) ) );
    sb_service_blind( $conf_global_ref,
                      $conf_blinds_ref,
                      $conf_blinds_ref->{'blinds'}->[$blind_index],
                      $state_calc_desired_blinds_states_ref->{'blinds'}->[$blind_index],
                                  $state_service_blinds_ref->{'blinds'}->[$blind_index],
                      $state_calc_desired_blinds_states_ref->{'datetime'} );
  }
}


sub read_conf
{
  my ($qualified_filename) = @_;

  my $base_dir = $qualified_filename;
  $base_dir =~ s|\\|/|g;
  $base_dir =~ s|/[^/]+$||;

  my $json_content = read_file($qualified_filename);
  my $g_conf_ref = decode_json($json_content);
  $g_conf_ref->{'global'}->{'base_dir'}=$base_dir;
 # printf("read_conf $g_conf_ref->{global}->{base_dir}\n");
  return $g_conf_ref;
}

my $state_filename;

sub read_state
{
  my ($conf_ref) = @_;
 # $state_filename = "$conf_ref->{global}->{base_dir}/$conf_ref->{global}->{workspace_dir}/blinds.state";
  $state_filename = "$conf_ref->{global}->{workspace_dir}/blinds.state";
  if( !(-e $state_filename) )
  {
    return
    {
      'get_weather' => {},
      'calc_desired_irradiance' => {},
      'calc_irradiance' => {},
      'calc_desired_blinds_states' => {},
      'service_blinds' => {},
    };
  }
  die if(-d $state_filename);

  my $json_content = read_file($state_filename);
  my $state_ref    = decode_json($json_content);
  $state_ref->{'get_weather'}={}                if(!exists($state_ref->{'get_weather'}));
  $state_ref->{'calc_desired_irradiance'}={}    if(!exists($state_ref->{'calc_desired_irradiance'}));
  $state_ref->{'calc_irradiance'}={}            if(!exists($state_ref->{'calc_irradiance'}));
  $state_ref->{'calc_desired_blinds_states'}={} if(!exists($state_ref->{'calc_desired_blinds_states'}));
  $state_ref->{'service_blinds'}={}             if(!exists($state_ref->{'service_blinds'}));
  return $state_ref;
}

sub write_state
{
  my ($state_ref) = @_;
  my $json_content =$json_coder->encode($state_ref);
  write_file( $state_filename, \$json_content );
}

sub date_time_string
{
  my ($time_forecast_io) = @_;

  my @mns = ('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
  my ($second, $minute, $hour, $day_of_month, $month_minus_one, $year_minus_1900, $weekday, $day_of_year_minus_one, $isdst) = localtime($time_forecast_io);
  my $tz = strftime("%Z", localtime($time_forecast_io));
  my $month_str = $mns[$month_minus_one];
  my $day_of_year = $day_of_year_minus_one + 1;
  my $year = $year_minus_1900 + 1900;
  
  return sprintf("%.4d-%.3d_%s-%.2d_%.2d%.2d%s", $year, $day_of_year, $month_str, $day_of_month, $hour, $minute, $tz);
}

sub read_file
{
  my ($file_name) = @_;

  my $contents;
  open(FLIN, $file_name) or die("Can not open $file_name\n");
  binmode(FLIN);
  read(FLIN, $contents, -s  FLIN);
  close(FLIN);

  return $contents;
}

sub write_file
{
  my ($file_name, $contents_ref) = @_;

  my $contents;
  open(FLOUT, ">$file_name") or die("Can not open $file_name\n");
  binmode(FLOUT);
  print(FLOUT ${$contents_ref});
  close(FLOUT)
}

1;
