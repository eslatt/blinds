#!/usr/bin/perl
use strict;

use Astro::Coord::ECI;
use Astro::Coord::ECI::Sun;
use Astro::Coord::ECI::Utils qw{:all};
use POSIX;

use JSON::XS;
my $json_coder = JSON::XS->new->pretty(1);
$json_coder->canonical(1);

use LWP::UserAgent;
use HTTP::Request::Common qw(GET POST);

my $g_agent = new LWP::UserAgent;

 # my $json_content = read_file($input_filename);
 # my $g_bj_ref = decode_json($json_content);

printf<<end_of_literal;

 control

end_of_literal

my $action='http://api.forecast.io/forecast/c02c92892be6e2d9a4c6e19104c4d614/40.351366,-76.660661';
my $req;
my $res;
my $json_content;

$req = GET( $action );
$res = $g_agent->request($req);
die("    failed to get $action\n")if(!$res->is_success);
$json_content = $res->decoded_content;
my $forecast_ref = decode_json($json_content);

my $max_temp_today_f = $forecast_ref->{'daily'}->{'data'}->[0]->{'temperatureMax'};
my $max_temp_time_today = date_time_string($forecast_ref->{'daily'}->{'data'}->[0]->{'temperatureMaxTime'});
my $cloud_coverage_now = $forecast_ref->{'currently'}->{'cloudCover'};
$cloud_coverage_now=0;
my $pressure_now = $forecast_ref->{'currently'}->{'pressure'};
my $sunrise_time_today = date_time_string($forecast_ref->{'daily'}->{'data'}->[0]->{'sunriseTime'});
my $sunset_time_today = date_time_string($forecast_ref->{'daily'}->{'data'}->[0]->{'sunsetTime'});

my $dlat = 40.351366;
my $dlon = -76.660661;

my $lat = deg2rad ($dlat);  # Radians
my $lon = deg2rad ($dlon);  # Radians
my $alt = 146 / 1000;       # Kilometers

sub my_time
{
  return time();
}
 # works
 # my $time_fcn = sub { return time(); };
 
 # breaks
 # my $time_fcn = \&time;
 # my $time_fcn = 'time';

 # test
 my $time_fcn = \&my_time;

# my $test_time = $time_fcn->();
 # my $test_time = time();
  my $test_time = 1434888036;
printf(" test time %d %s\n", $test_time, date_time_string($test_time));

# my $loc = Astro::Coord::ECI->geodetic( $lat, $lon, $alt );
my $loc = Astro::Coord::ECI->universal(  $test_time )->geodetic ($lat, $lon, $alt);

# my $sun = Astro::Coord::ECI::Sun->new ();
my $sun = Astro::Coord::ECI::Sun->universal(  $test_time );

my ($azimuth_rad, $elevation_rad, $range) = $loc->azel( $sun ); # north is 0 degrees

my $azimuth_deg   = rad2deg($azimuth_rad);
my $elevation_deg = rad2deg($elevation_rad);

my $zenith_rad                  = -1;
my $zenith_deg                  = -1;
my $air_mass                    = -1;
my $irradiance_direct_beam_wpm2 = -1;
my $irradiance_clouded_wpm2     = -1;
my $irradiance_window_wpm2      = -1;
my $irradiance_special_wpm2     = -1;

if( $elevation_rad > 0 )
{
 # calc zenith
  $zenith_rad = PI/2 - $elevation_rad;
  $zenith_deg = rad2deg($zenith_rad);

 # calc airmass ( 96.07995 degrees is 1.676911 radiance )
  $air_mass = 1 / ( cos( $zenith_rad ) + 0.50572 * pow( rad2deg( 1.676911 - $zenith_rad ), -1.6364 ) );

 # calc irradiance
  $irradiance_direct_beam_wpm2 = 1353 * ( ( 1 - 0.14 * $alt ) * pow( 0.7, pow( $air_mass, 0.678 ) ) +  0.14 * $alt );

  my $blinds_elevation_deg = 0;
  my $blinds_azimuth_deg = 198;
  my $blinds_elevation_rad = deg2rad($blinds_elevation_deg);
  my $blinds_azimuth_rad = deg2rad($blinds_azimuth_deg);

  my $sun_x = cos($elevation_rad)*sin($azimuth_rad);
  my $sun_y = cos($elevation_rad)*cos($azimuth_rad);
  my $sun_z = sin($elevation_rad);
  
  my $blinds_x = cos($blinds_elevation_rad)*sin($blinds_azimuth_rad);
  my $blinds_y = cos($blinds_elevation_rad)*cos($blinds_azimuth_rad);
  my $blinds_z = sin($blinds_elevation_rad);
  
  my $special_window_x = cos(0)*sin($azimuth_rad);
  my $special_window_y = cos(0)*cos($azimuth_rad);
  my $special_window_z = sin(0);

  
  my $blinds_dot_sun  = $sun_x * $blinds_x         + $sun_y * $blinds_y         + $sun_z *$blinds_z;
  my $special_dot_sun = $sun_x * $special_window_x + $sun_y * $special_window_y + $sun_z *$special_window_z;

  $irradiance_clouded_wpm2 = ( 1 - $cloud_coverage_now ) * $irradiance_direct_beam_wpm2;
  $irradiance_window_wpm2 = $blinds_dot_sun * $irradiance_clouded_wpm2;
  $irradiance_special_wpm2 = $special_dot_sun * $irradiance_clouded_wpm2;
}


printf<<end_of_literal;

  cloud_coverage_now                  $cloud_coverage_now
  pressure                            $pressure_now
  max_temp_today_f                    $max_temp_today_f
  max_temp_time_today                 $max_temp_time_today
  
  current azimuth_deg                 $azimuth_deg
  current elevation_deg               $elevation_deg
  current elevation_rad               $elevation_rad
  current zenith_deg                  $zenith_deg
  current air_mass                    $air_mass
  current irradiance_direct_beam_wpm2 $irradiance_direct_beam_wpm2
  current irradiance_clouded_wpm2     $irradiance_clouded_wpm2
  current irradiance_window_wpm2      $irradiance_window_wpm2
  current irradiance_special_wpm2     $irradiance_special_wpm2

end_of_literal

$json_content =$json_coder->encode($forecast_ref);
write_file("response", \$json_content);

printf<<end_of_literal;

 Thank you

end_of_literal

sub date_time_string
{
  my ($time_forecast_io) = @_;
  
  my @mns = ('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
  my ($second, $minute, $hour, $day_of_month, $month_minus_one, $year_minus_1900, $weekday, $day_of_year_minus_one, $isdst) = 
    localtime($time_forecast_io);
  my $month_str = $mns[$month_minus_one];
  my $day_of_year = $day_of_year_minus_one + 1;
  my $year = $year_minus_1900 + 1900;
  return sprintf("%.4d-%.3d_%s-%.2d_%.2d-%.2d", $year, $day_of_year, $month_str, $day_of_month, $hour, $minute);
}

sub read_file
{
  my ($file_name) = @_;

  my $contents;
  open(FLIN, $file_name) or die("Can not open $file_name\n");
  binmode(FLIN);
  read(FLIN, $contents, -s  FLIN);
  close(FLIN);

  return $contents;
}

sub write_file
{
  my ($file_name, $contents_ref) = @_;

  my $contents;
  open(FLOUT, ">$file_name") or die("Can not open $file_name\n");
  binmode(FLOUT);
  print(FLOUT ${$contents_ref});
  close(FLOUT)
}