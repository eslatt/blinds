#!/usr/bin/perl
use strict;

use JSON::XS;
my $json_coder = JSON::XS->new->pretty(1);
$json_coder->canonical(1);

use LWP::UserAgent;
use HTTP::Request::Common qw(GET POST);

my $g_agent = new LWP::UserAgent;

 # my $json_content = read_file($input_filename);
 # my $g_bj_ref = decode_json($json_content);

printf<<end_of_literal;

 weather data

end_of_literal

my $action='http://api.forecast.io/forecast/c02c92892be6e2d9a4c6e19104c4d614/40.351366,-76.660661';
my $req;
my $res;
my $json_content;

$req = GET( $action );
$res = $g_agent->request($req);
die("    failed to get $action\n")if(!$res->is_success);
$json_content = $res->decoded_content;
my $forecast_ref = decode_json($json_content);

my $max_temp_f_today = $forecast_ref->{'daily'}->{'data'}->[0]->{'temperatureMax'};
my $max_temp_time_today = date_time_string($forecast_ref->{'daily'}->{'data'}->[0]->{'temperatureMaxTime'});
my $cloud_coverage_now = $forecast_ref->{'currently'}->{'cloudCover'};
my $pressure_now = $forecast_ref->{'currently'}->{'pressure'};
my $sunrise_time_today = date_time_string($forecast_ref->{'daily'}->{'data'}->[0]->{'sunriseTime'});
my $sunset_time_today = date_time_string($forecast_ref->{'daily'}->{'data'}->[0]->{'sunsetTime'});

printf<<end_of_literal;

  cloud_coverage_now  $cloud_coverage_now
  pressure            $pressure_now
  max_temp_f_today    $max_temp_f_today
  max_temp_time_today $max_temp_time_today
  sunrise_time_today  $sunrise_time_today
  sunset_time_today   $sunset_time_today

end_of_literal

$json_content =$json_coder->encode($forecast_ref);
write_file("response", \$json_content);

printf<<end_of_literal;

 Thank you

end_of_literal

sub date_time_string
{
  my ($time_forecast_io) = @_;
  
  my @mns = ('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
  my ($second, $minute, $hour, $day_of_month, $month_minus_one, $year_minus_1900, $weekday, $day_of_year_minus_one, $isdst) = 
    localtime($time_forecast_io);
  my $month_str = $mns[$month_minus_one];
  my $day_of_year = $day_of_year_minus_one + 1;
  my $year = $year_minus_1900 + 1900;
  return sprintf("%.4d-%.3d_%s-%.2d_%.2d-%.2d", $year, $day_of_year, $month_str, $day_of_month, $hour, $minute);
}

sub read_file
{
  my ($file_name) = @_;

  my $contents;
  open(FLIN, $file_name) or die("Can not open $file_name\n");
  binmode(FLIN);
  read(FLIN, $contents, -s  FLIN);
  close(FLIN);

  return $contents;
}

sub write_file
{
  my ($file_name, $contents_ref) = @_;

  my $contents;
  open(FLOUT, ">$file_name") or die("Can not open $file_name\n");
  binmode(FLOUT);
  print(FLOUT ${$contents_ref});
  close(FLOUT)
}