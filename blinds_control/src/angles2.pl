#! /usr/bin/perl

use strict;

use Astro::Coord::ECI;
use Astro::Coord::ECI::Sun;
use Astro::Coord::ECI::Utils qw{:all};
use POSIX;

my $dlat = 40.351366;
my $dlon = -76.660661;

my $lat = deg2rad ($dlat);  # Radians
my $lon = deg2rad ($dlon);  # Radians
my $alt = 146 / 1000;       # Kilometers

my $test_time = time();
 # my $test_time = 1430150436;
printf(" test time %d %s\n", $test_time, date_time_string($test_time));

# my $loc = Astro::Coord::ECI->geodetic( $lat, $lon, $alt );
my $loc = Astro::Coord::ECI->universal(  $test_time )->geodetic ($lat, $lon, $alt);

# my $sun = Astro::Coord::ECI::Sun->new ();
my $sun = Astro::Coord::ECI::Sun->universal(  $test_time );

my ($azimuth_rad, $elevation_rad, $range) = $loc->azel( $sun ); # north is 0 degrees

my $azimuth_deg   = rad2deg($azimuth_rad);
my $elevation_deg = rad2deg($elevation_rad);
printf(" current azimuth_deg   %f\n", $azimuth_deg);
printf(" current elevation_deg  %f\n", $elevation_deg);

my $zenith_rad                  = -1;
my $zenith_deg                  = -1;
my $air_mass                    = -1;
my $irradiance_direct_beam_wpm2 = -1;
my $irradiance_clouded_wpm2     = -1;
my $irradiance_window_wpm2      = -1;

printf(" current elevation_rad  %.0f\n", $elevation_rad);
if( $elevation_rad > 0 )
{
 # calc zenith
  $zenith_rad = PI/2 - $elevation_rad;
  $zenith_deg = rad2deg($zenith_rad);

 # calc airmass ( 96.07995 degrees is 1.676911 radiance )
  my $pressure = 1007.67;
  $air_mass = 1 / ( cos( $zenith_rad ) + 0.50572 * pow( rad2deg( 1.676911 - $zenith_rad ), -1.6364 ) );

 # calc irradiance
  $irradiance_direct_beam_wpm2 = 1353 * ( ( 1 - 0.14 * $alt ) * pow( 0.7, pow( $air_mass, 0.678 ) ) +  0.14 * $alt );

  my $blinds_elevation_deg = 10;
  my $blinds_azimuth_deg = 182;
  my $blinds_elevation_rad = deg2rad($blinds_elevation_deg);
  my $blinds_azimuth_rad = deg2rad($blinds_azimuth_deg);

  my $sun_x = cos($elevation_rad)*sin($azimuth_rad);
  my $sun_y = cos($elevation_rad)*cos($azimuth_rad);
  my $sun_z = sin($elevation_rad);
  
  my $blinds_x = cos($blinds_elevation_rad)*sin($blinds_azimuth_rad);
  my $blinds_y = cos($blinds_elevation_rad)*cos($blinds_azimuth_rad);
  my $blinds_z = sin($blinds_elevation_rad);
  
  my $blinds_dot_sun = $sun_x * $blinds_x + $sun_y * $blinds_y + $sun_z *$blinds_z;
 # printf(" current blinds_dot_sun  %f\n", $blinds_dot_sun);
 # printf("  sun_x $sun_x\n" );
 # printf("  sun_y $sun_y\n" );
 # printf("  sun_z $sun_z\n" );
 # printf("  blinds_x $blinds_x\n" );
 # printf("  blinds_y $blinds_y\n" );
 # printf("  blinds_z $blinds_z\n" );
  my $cloud_coverage = 1;
  $irradiance_clouded_wpm2 = $cloud_coverage * $irradiance_direct_beam_wpm2;
  $irradiance_window_wpm2 = $blinds_dot_sun * $irradiance_clouded_wpm2;
}

printf(" current zenith_deg  %.0f\n", $zenith_deg);
printf(" current air_mass  %f\n", $air_mass);
printf(" current irradiance_direct_beam_wpm2  %f\n", $irradiance_direct_beam_wpm2);
printf(" current irradiance_clouded_wpm2  %f\n", $irradiance_clouded_wpm2);
printf(" current irradiance_window_wpm2  %f\n", $irradiance_window_wpm2);


my ($time, $rise, $rise_datetime);

($time, $rise) = $loc->next_elevation ($sun);
$rise_datetime = date_time_string($time);
printf(" sun @{[$rise ? 'rise' : 'set ']} $rise_datetime\n");

($time, $rise) = $loc->next_elevation ($sun);
$rise_datetime = date_time_string($time);
printf(" sun @{[$rise ? 'rise' : 'set ']} $rise_datetime\n");

sub date_time_string
{
  my ($time_forecast_io) = @_;

  my @mns = ('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
  my ($second, $minute, $hour, $day_of_month, $month_minus_one, $year_minus_1900, $weekday, $day_of_year_minus_one, $isdst) =
    localtime($time_forecast_io);
  my $month_str = $mns[$month_minus_one];
  my $day_of_year = $day_of_year_minus_one + 1;
  my $year = $year_minus_1900 + 1900;
  return sprintf("%.4d-%.3d_%s-%.2d_%.2d-%.2d", $year, $day_of_year, $month_str, $day_of_month, $hour, $minute);
}