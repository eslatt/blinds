docker run -it -v "$(pwd)/":/code registry.gitlab.com/eslatt/blinds:latest bash

docker build -t registry.gitlab.com/eslatt/blinds .
docker push registry.gitlab.com/eslatt/blinds
docker pull registry.gitlab.com/eslatt/blinds


yum -y group install "Development Tools"
# yum -y install perl
yum -y install cpanminus
yum -y install perl-Test-Simple
cpanm -y JSON::XS
cpanm -y LWP::UserAgent

cpanm -y Astro::App::Satpass2
cpanm -y IO::Socket::SSL
cpanm -y LWP::Protocol::https