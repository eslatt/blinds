use strict;

printf<<end_of_literal;
  solar angles test
end_of_literal

 use Astro::Coord::ECI;
 use Astro::Coord::ECI::Sun;
 use Astro::Coord::ECI::Utils qw{deg2rad};
 
 # 1600 Pennsylvania Ave, Washington DC USA
 # latitude 38.899 N, longitude 77.038 W,
 # altitude 16.68 meters above sea level
 # 111 crabapple 40.351366,-76.660661
 my $lat = deg2rad (40.351366);    # Radians
 my $long = deg2rad (-76.660661);  # Radians
 my $alt = 146 / 1000;        # Kilometers
 my $sun = Astro::Coord::ECI::Sun->new ();
 my $sta = Astro::Coord::ECI->
     universal (time ())->
     geodetic ($lat, $long, $alt);
 my ($time, $rise) = $sta->next_elevation ($sun);
 print "Sun @{[$rise ? 'rise' : 'set']} is ",
     scalar gmtime $time, " UT\n";
  
  my $sunset_datetime = date_time_string($time);
  printf("$sunset_datetime\n");
 
sub date_time_string
{
  my ($time_forecast_io) = @_;
  
  my @mns = ('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
  my ($second, $minute, $hour, $day_of_month, $month_minus_one, $year_minus_1900, $weekday, $day_of_year_minus_one, $isdst) = 
    localtime($time_forecast_io);
  my $month_str = $mns[$month_minus_one];
  my $day_of_year = $day_of_year_minus_one + 1;
  my $year = $year_minus_1900 + 1900;
  return sprintf("%.4d-%.3d_%s-%.2d_%.2d-%.2d", $year, $day_of_year, $month_str, $day_of_month, $hour, $minute);
}