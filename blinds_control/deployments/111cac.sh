#!/bin/bash

# convenient to have these set in here because env vars are weird with crontab
blinds_src_dir=/home/blinds/blinds/blinds_control/src
blinds_log_dir=/home/blinds/blinds_workspace
# is there a way to get the directory that this sh file sits in?
blinds_conf_qualified_filename=/home/blinds/blinds/blinds_control/deployments/111cac.conf

cd $blinds_src_dir
$blinds_src_dir/blinds_control_p1.pl $blinds_conf_qualified_filename >> $blinds_log_dir/blinds.log
