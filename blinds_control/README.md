## Algorithm

Every five minutes it pulls the weather and decides whether it thinks the blinds should be open or closed.

### Actors

#### independent of window orientation
##### temp
This is the predicted max temp for the day and typically doesn't change during the day
##### desired_irradiance_wmp2
This is a linear function of temp and defined by parameters in the conf
```
   "desired_irradiance" : {
      "estimated_temp_f_1" : 75,
      "desired_irradiance_wmp2_1" : 170,
      "estimated_temp_f_2" : 80,
      "desired_irradiance_wmp2_2" : 100
   }
```
##### irradiance_direct_beam_wpm2
This gives watts per meter squared given the amount of atmosphere the sun has to go through to get to the window.  In other words, you can look directly at the sun at sunrise and sunset because its diminished by all the air it has gone through but you can't look at it at noon because it has gone through much less air.  This is a function of only elevation and set to -1 if the sun is below the horizon.
##### elevation_rad
angle of the sun above the horizon
##### irradiance_clouded_wpm2
* reduces the direct beam by cloud coverage percentage
* this isn't in the proper data object location and is queued to be fixed in [issue 1](https://gitlab.com/eslatt/blinds/-/issues/1)

#### dependent on window orientation

##### irradiance_window_wpm2
##### desired_light_state

## questions
why don't I calculate clouded wpm2 before going to window specific?  - its a screw up

## options

### shaded

When shaded is set to true for a window, desired_light_state is set to 1 independent of the result of the irradiance_window_wpm2 calcualtion.  That calculation is still performed though because its sometimes useful to graph it to understand what's going on.
<img height=200 src="docs/images/before-shaded.png"/> <img height=200 src="docs/images/after-shaded.png"/>

The reason I think the north window is shaded is that early in the morning, the elevation of the sun is very low.  I never recall direct sunlight in the office and I think its because the sun is shaded by the treeline that early in the morning.
