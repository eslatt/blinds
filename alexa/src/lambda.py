import json

response_tmpl = {
  "version": "1.0",
  "response": {
    "outputSpeech": {
      "type": "PlainText",
      "text": "hello from python",
    },
    "shouldEndSession": True
  }
}

def lambda_handler (event, context):
    response_tmpl["debug"]=dict()
    response_tmpl["debug"]["test"]="test"
    response_tmpl["debug"]["request type"]=event["request"]["type"]
#    response_tmpl["debug"]["context session id"]=context["SessionID"]

    if(event["request"]["intent"]["name"] != "LowerBlindsIntent"):
        response_tmpl["response"]["outputSpeech"]["text"]="only the following is supported one please lower the blinds"
        response_tmpl["debug"]["intent"]=event["request"]["intent"]["name"]
        return response_tmpl

    if(event["request"]["intent"]["slots"]["ValidationBlinds"]["value"] != "blinds"):
        response_tmpl["response"]["outputSpeech"]["text"]="only the following is supported two please lower the blinds"
        return response_tmpl    
    
    if(event["request"]["intent"]["slots"]["ValidationRaiseLower"]["value"] == "lower"):
        response_tmpl["response"]["outputSpeech"]["text"]="lowering the blinds now"
        return response_tmpl
    elif(event["request"]["intent"]["slots"]["ValidationRaiseLower"]["value"] == "raise"):
        response_tmpl["response"]["outputSpeech"]["text"]="raising the blinds now"
        return response_tmpl

    response_tmpl["response"]["outputSpeech"]["text"]="only the following is supported three please lower the blinds"
    return response_tmpl
