## Background

This project started in 2015 and included mechanical, electrical and software design elements required for smart blinds.  The control elements are in the blinds_control directory.

WIP - I'm moving to new deployments directory in branch 2022-06-north-support

## Blinds Control Configuration

clone the entire repo on the machine that will run the automation

there's a command script, config file, state file and log file

* version controlled command script is called from crontab entries as shown below
  * it lives in *blinds/blinds_control/deployements*

* command script names a version controlled config file is as shown below
* the state file has a hardwired name  `blinds.state` and is located in the workspace directory, which is defined in the config file as shown below
* configured to be outside of the version controlled repo

``` crontab -l
# first run raises blinds at 7:20am on Mon through Fri
20  7  * * 1-5 /root/blinds/blinds_control/deployments/111cac.sh
# first run raise blinds at 8:30am on saturday and sunday
30  8  * * 0,6 /root/blinds/blinds_control/deployments/111cac.sh
# do the control every five minutes from 9am to 10:55pm (4pm in winter)
*/5 16-22 * * * /root/blinds/blinds_control/deployments/111cac.sh
```

``` /root/blinds/blinds_control/src/blinds_control_p1.sh
#!/bin/bash

# convenient to have these set in here because env vars are weird with crontab
blinds_src_dir=/home/blinds/blinds/blinds_control/src
blinds_log_dir=/home/blinds/blinds_workspace
# is there a way to get the directory that this sh file sits in?
blinds_conf_qualified_filename=/home/blinds/blinds/blinds_control/deployments/111cac.conf

cd $blinds_src_dir
$blinds_src_dir/blinds_control_p1.pl $blinds_conf_qualified_filename >> $blinds_log_dir/blinds.log

```

``` rt/blinds.conf
{
   "global" : {
      "loc_lat_degrees" : 40.351366,
      "loc_lon_degrees" : -76.660661,
      "loc_alt_km" : 0.146,
      "workspace_dir" : "/root/blinds_workspace",
      "canned_weather_dir" : "e:/ess/projects/blinds/blinds_control/src/rt/weather"
   },
   "desired_irradiance" : {
      "estimated_temp_f_1" : 75,
      "desired_irradiance_wmp2_1" : 170,
      "estimated_temp_f_2" : 80,
      "desired_irradiance_wmp2_2" : 100
   },
   "blinds" : {
      "dwell_wmp2" : 100,
      "dwell_seconds" : 1800,
      "blinds" : [
         {
            "security_min_elevation_degrees" : -3.5,
            "azimuth_degrees" : 198,
            "elevation_degrees" : 0,
            "light_command" : "/root/XbeeGateway/light",
            "dark_command" : "/root/XbeeGateway/dark"
         }
      ]
   }
}
```

## Dependencies (Centos 7.6)

* (not needed on Iroh) yum group install "Development Tools"
* (not needed on Iroh) yum install perl
* yum install cpanminus
* yum install perl-Test-Simple
* cpanm JSON::XS
* cpanm LWP::UserAgent
* cpanm Astro::App::Satpass2
* cpanm IO::Socket::SSL
* cpanm LWP::Protocol::https

## Installation

install dependencies

clone to the target machine

create a workspace directory

create version controlled conf file in  `blinds_control/deployments` like  `111cac.conf`

* `workspace_dir`
* `light_command`
* `dark_command`

create version controlled command script in `blinds_control/deployments` like `111cac.sh`

* blinds_src_dir=<clone/>/blinds_control/src
* blinds_log_dir=<workspace/>
* blinds_conf_qualified_filename=<fully qualified conf filename/>

install the crontab entries
* `crontab blinds_control/deployments/111cac.crontab`

see it work as per below

create crontab entries as per above

## To Update

make the desired commit current on the control server

update the cron if the version controlled crontab file changes
* `crontab blinds_control/deployments/111cac.crontab`

ensure the following files are +x
* blinds_control/deployments/111cac.sh
* blinds_control/src/blinds_control_p1.pl
* open and close scripts referenced by the conf file

##  To See the Thing Work

to see the thing work, remove state file, invoke the automation using the crontab'd command and see that the state file is recreated

Or.. invoke the office close command.. then go into the state file and set it to close.. and watch the next cron invocation open it back up.

``` blinds.state
{
   "calc_desired_blinds_states" : {
      "blinds" : [
         {
            "desired_light_state" : 1,
            "irradiance_window_wpm2" : 0
         }
      ],
      "datetime" : 1652872802,
      "irradiance_clouded_wpm2" : 554.828552316413
   },
   "calc_desired_irradiance" : {
      "desired_irradiance_wmp2" : 241.96
   },
   "calc_irradiance" : {
      "air_mass" : 3.642732610189,
      "azimuth_rad" : 1.35029099421644,
      "datetime" : "1652872802",
      "elevation_rad" : "0.27470246302819",
      "i_unit" : 0.939200838167436,
      "irradiance_direct_beam_wpm2" : 590.243140762141,
      "j_unit" : 0.210521946383168,
      "k_unit" : 0.271260567861655,
      "zenith_rad" : 1.29609386376671
   },
   "get_weather" : {
      "cloud_coverage_now" : 0.06,
      "datetime" : 1652872802,
      "max_temp_f_today" : 69.86,
      "max_temp_time_today" : "2022-138_May-18_17-57",
      "pressure_now" : 1018,
      "sunrise_time_today" : "2022-138_May-18_05-49",
      "sunset_time_today" : "2022-138_May-18_20-20"
   },
   "service_blinds" : {
      "blinds" : [
         {
            "last_command" : {
               "datetime" : 1652872802,
               "irradiance_wmp2" : 0,
               "light" : 1
            }
         }
      ]
   }
}
```

## [Troubleshooting](troubleshooting.md)

