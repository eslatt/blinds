#include <pins_arduino.h>
#include <EEPROM.h>

// configuration constants
#define MOMENTS_PER_STEP         3 // gives inverse speed
#define STEPS_PER_INCH        1000
#define WINDOW_HEIGHT_INCH      60

// arduino pins 4, 5 and 6 are spent programming 
int coil_state = 0;
int coil_apins[] = { 10, 9, 1, 0 };

#define DIR_APIN 2

int tick_state  = 0;

int inverse_speed = 3;

void setup()
{
  pinMode( DIR_APIN,      INPUT_PULLUP );
  pinMode( coil_apins[0], OUTPUT );
  pinMode( coil_apins[1], OUTPUT );
  pinMode( coil_apins[2], OUTPUT );
  pinMode( coil_apins[3], OUTPUT );
 }

void loop()
{
  digitalWrite(coil_apins[coil_state], LOW);
  if(digitalRead(DIR_APIN))
    coil_state++;
  else
    coil_state--;
  coil_state+=8;
  coil_state = coil_state % 4;
  digitalWrite(coil_apins[coil_state], HIGH);

  delay(inverse_speed);
}

void loop_v1()
{
 // adjust the motor state on
 // digitalWrite(apin, HIGH);
 // digitalWrite(3, LOW);
 // delay(50);
  digitalWrite(coil_apins[0], HIGH);
  delay(inverse_speed);
  digitalWrite(coil_apins[0], LOW);
  digitalWrite(coil_apins[1], HIGH);
  delay(inverse_speed);
  digitalWrite(coil_apins[1], LOW);
  digitalWrite(coil_apins[2], HIGH);
  delay(inverse_speed);
  digitalWrite(coil_apins[2], LOW);
  digitalWrite(coil_apins[3], HIGH);
  delay(inverse_speed);
  digitalWrite(coil_apins[3], LOW);
}
