#include <pins_arduino.h>
#include <EEPROM.h>

 // configuration constants
#define BIPOLAR_STEPPER                      // UNIPOLAR_STEPPER has 5 wires, BIPOLAR_STEPPER has 4 wires
#define MOMENTS_PER_PAUSE           1000
#define STEP_TOWARD_LIGHT_DIRECTION    1.0f
#define WINDOW_THROW_HEIGHT_INCH       59.5f // 0.5f // 59.5f

#ifdef BIPOLAR_STEPPER
long moments_per_step_dark;
long moments_per_step_light;
  #define STEPS_PER_INCH              8422.0f // v3-8563.6f // v2-7844.0f // v1-10458.0f
#else
  #define STEPS_PER_INCH              5229.0f
  #define MOMENTS_PER_STEP_DARK        425    // 425 gives ~10 seconds for .5"; works around clutch issue; light direction is afasp, no delay
#endif
 // #define MOMENTS_PER_STEP_DARK           20000    // to test stepper sequency, set to 20000 and pull DARK_SWITCH_APIN_AL low before startup

 // active low (_AL) because using INPUT_PULLUP
#define LIGHT_SWITCH_APIN_AL  2
#define DARK_SWITCH_APIN_AL   3
#define LIGHT_XBEE_APIN_AH    8
#define DARK_XBEE_APIN_AH     7

 // digitalRead active low
#define DIGITAL_READ_AL !digitalRead
 // digitalRead active high
#define DIGITAL_READ_AH  digitalRead

struct Eeprom{
  double light_percent;
  int total_writes;
};

Eeprom persistent_data;

double current_light_percent = 0;

int lines_state = 0;
int line_apins[]    = { 10, 9, 1, 0 }; // arduino pins 4, 5 and 6 are spent programming

#ifdef BIPOLAR_STEPPER

// state | line 0 | line 1 | line 2 | line 3
//   0   |   x    |   o    |   x    |   o 
//   1   |   x    |   o    |   o    |   x 
//   2   |   o    |   x    |   o    |   x 
//   3   |   o    |   x    |   x    |   o 

#define TOTAL_COIL_STATES   4
int lines_high[][2] = { {0,2}, {0,3}, {1,3}, {1,2} };
int lines_low [][2] = { {1,3}, {1,2}, {0,2}, {0,3} };

#else

// half step states
// state | coil 0 | coil 1 | coil 2 | coil 3
//   0   |   x    |   o    |        |   o 
//   1   |   x    |   x    |   o    |   o 
//   2   |   o    |   x    |   o    |     
//   3   |   o    |   x    |   x    |   o 
//   4   |        |   o    |   x    |   o 
//   5   |   o    |   o    |   x    |   x 
//   6   |   o    |        |   o    |   x 
//   7   |   x    |   o    |   o    |   x 

#define TOTAL_COIL_STATES   8
int lines_high[][2] = { {0,0}, {0,1}, {1,1}, {1,2}, {2,2}, {2,3}, {3,3}, {3,0} };
int lines_low [][2] = { {3,1}, {3,2}, {0,2}, {0,3}, {1,3}, {1,0}, {2,0}, {2,1} };

#endif

void write_current_light_percent()
{
 // if( current_light_percent == persistent_data.light_percent )
 //   return;
 // persistent_data.light_percent = current_light_percent;
 // persistent_data.total_writes++;
 // EEPROM.put( 0, persistent_data );
}

void read_current_light_percent()
{
 // EEPROM.get( 0, persistent_data );
  
 // if(persistent_data.light_percent<0)
 //   persistent_data.light_percent=0;
 // else if(persistent_data.light_percent>1)
 //   persistent_data.light_percent=1;

 // current_light_percent=persistent_data.light_percent;
}

void move_to_light(double light_percent)
{
  bool   toward_light;
  int    stop_apin_al;

  double to_move_percent;
  double to_move_inches;
  long   to_move_steps;
  long   actually_moved_steps;
  double actually_moved_inches;
  double actually_moved_percent;

  to_move_percent = light_percent - current_light_percent;
  to_move_inches  = to_move_percent * WINDOW_THROW_HEIGHT_INCH;
  to_move_steps   = to_move_inches * STEPS_PER_INCH;

  toward_light = true;
  stop_apin_al = DARK_SWITCH_APIN_AL;
  if( to_move_steps < 0 )
  {
    toward_light = false;
    stop_apin_al = LIGHT_SWITCH_APIN_AL;
    to_move_steps*=-1;
  }

  actually_moved_steps = 0;
  while( !DIGITAL_READ_AL(stop_apin_al) &&
         actually_moved_steps < to_move_steps )
  {
    step(toward_light);
    actually_moved_steps++;
  }
  turn_motor_off();
  if(actually_moved_steps < to_move_steps)
    delay(MOMENTS_PER_PAUSE);

  if( !toward_light )
    actually_moved_steps*=-1;

  actually_moved_inches  = actually_moved_steps / STEPS_PER_INCH;
  actually_moved_percent = actually_moved_inches / WINDOW_THROW_HEIGHT_INCH;

  current_light_percent += actually_moved_percent;
  write_current_light_percent();
}

int noop=0;
void step(bool toward_light)
{
  if(toward_light)
    lines_state+=STEP_TOWARD_LIGHT_DIRECTION;
  else
    lines_state-=STEP_TOWARD_LIGHT_DIRECTION;
  lines_state+=2*TOTAL_COIL_STATES;
  lines_state = lines_state % TOTAL_COIL_STATES;
  digitalWrite(line_apins[lines_low[lines_state][0]], LOW);
  digitalWrite(line_apins[lines_low[lines_state][1]], LOW);
  digitalWrite(line_apins[lines_high [lines_state][0]], HIGH);
  digitalWrite(line_apins[lines_high [lines_state][1]], HIGH);

#ifdef MOMENTS_PER_STEP_DARK
   if(!toward_light)
   {
     for(noop=0;noop<MOMENTS_PER_STEP_DARK;noop++);
   }
#endif

//#ifdef MOMENTS_PER_STEP_LIGHT
   if(toward_light)
   {
     for(noop=0;noop<moments_per_step_light;noop++);
   }
//#endif

}

void turn_motor_off()
{
  digitalWrite(line_apins[lines_high [lines_state][0]], LOW);
  digitalWrite(line_apins[lines_high [lines_state][1]], LOW);
}

void move(bool toward_light)
{
  int go_apin_al;

  if( toward_light )
    go_apin_al = LIGHT_SWITCH_APIN_AL;
  else
    go_apin_al = DARK_SWITCH_APIN_AL;

  while( DIGITAL_READ_AL( go_apin_al ) )
    step( toward_light );

  turn_motor_off();
}

void setup()
{
  pinMode( LIGHT_SWITCH_APIN_AL, INPUT_PULLUP );
  pinMode( DARK_SWITCH_APIN_AL,  INPUT_PULLUP );
  pinMode( LIGHT_XBEE_APIN_AH,   INPUT_PULLUP );
  pinMode( DARK_XBEE_APIN_AH,    INPUT_PULLUP );
  pinMode( line_apins[0], OUTPUT );
  pinMode( line_apins[1], OUTPUT );
  pinMode( line_apins[2], OUTPUT );
  pinMode( line_apins[3], OUTPUT );
}

void loop()
{
  int i;
  
 // 5 slow steps
  moments_per_step_light=7500; // production fast
  for(i=0;i<50;i++)  
    step(1); // toward light

 // 500 fast steps
  moments_per_step_light=75; // production fast
  for(i=0;i<5000;i++)  
    step(1); // toward light
}


