package blinds_xbee;

import com.rapplogic.xbee.api.XBee;
import com.rapplogic.xbee.api.XBeeException;
import com.rapplogic.xbee.api.XBeeAddress64;
import com.rapplogic.xbee.XBeePin;
import com.rapplogic.xbee.api.RemoteAtRequest;
import com.rapplogic.xbee.api.RemoteAtResponse;

public class Blinds_xbee
{
  static XBee xbee;
  static XBeeAddress64 addr64;
  static int req_data_array[];

  static String LIGHT_XBEE_PIN_NAME_AL = "D0";
  static String DARK_XBEE_PIN_NAME_AL  = "D1";

  public static boolean send_pin_state( String xbee_pin_name, boolean state )
  {
    if(state)
      req_data_array[0]=XBeePin.Capability.DIGITAL_OUTPUT_HIGH.getValue();
    else
      req_data_array[0]=XBeePin.Capability.DIGITAL_OUTPUT_LOW.getValue();
    RemoteAtRequest request = new RemoteAtRequest(addr64, xbee_pin_name, req_data_array);

    try
    {
      xbee.sendAsynchronous(request);
    }
    catch (Exception exc)
    {
      System.out.println("failed to sendAsynchronous");
      return false;
    }

    try
    {
      RemoteAtResponse response = (RemoteAtResponse) xbee.getResponse();
      if (!response.isOk())
      {
       // System.out.println("Attempt to turn on DIO0 failed.  Status: " + response.getStatus());
        System.out.println("failed to set " + LIGHT_XBEE_PIN_NAME_AL + " to " + state + "; status " + response.getStatus());
        return false;
      }
    }
    catch (Exception exc)
    {
      System.out.println("Failed to get response");
      return false;
    }
    return true;
  }

  public static void main(String[] args)
  {
    boolean response;
    if (args.length != 2)
    {
      System.out.println("usage: blinds_xbee <com port name/> <light|dark/>");
      return;
    }
           
    String serial_port_name = args[0];
    String xbee_pin = LIGHT_XBEE_PIN_NAME_AL;
    if( args[1].equalsIgnoreCase("dark") )
      xbee_pin = DARK_XBEE_PIN_NAME_AL;  
    
    System.out.println("blinds xbee " + serial_port_name + " " + xbee_pin);
    
    xbee = new XBee();
    try
    {
      xbee.open( serial_port_name, 9600);
    }
    catch (Exception exc)
    {
      System.out.println("failed to open xbee on serial port " + serial_port_name);
      System.exit(0);
    }

   // this is the Serial High (SH) + Serial Low (SL) of the remote XBee
   // router 1 0013A20040C04A84 ,
    addr64 = new XBeeAddress64(0x00, 0x13, 0xA2, 0x00, 0x40, 0xC0, 0x4A, 0x84);

    req_data_array=new int[1];

    response = send_pin_state( xbee_pin, true );
    if (!response)
      System.exit(0);

    try
    {
      Thread.sleep(2000);
    }
    catch (Exception exc)
    {
      System.out.println("Failed to sleep");
    }

    response = send_pin_state( xbee_pin, false );
    if (!response)
      System.exit(0);

   // shutdown the serial port and associated threads
    xbee.close();
  }
}
